import cv2

"""
Function for creating a big rectangle around the forms content in order to close of the rectangles. Used for finding the
fields more accurately
"""


def boxification(image):
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    contours, _ = cv2.findContours(gray, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

    for contour in contours:
        x, y, w, h = cv2.boundingRect(contour)
        cv2.rectangle(image, (x + 320, y), (x + w - 340, y + h), (0, 0, 0), 100)

    return image
