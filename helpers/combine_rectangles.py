from typing import Tuple
import numpy as np

"""
Class for combing rectangles which are very close used for piecing possible found signature pieces together
"""


class CombineRectangles():

    @staticmethod
    def __rect_union(a: Tuple[int, int, int, int], b: Tuple[int, int, int, int]) -> Tuple[int, int, int, int]:
        x = min(a[0], b[0])
        y = min(a[1], b[1])
        w = max(a[0] + a[2], b[0] + b[2]) - x
        h = max(a[1] + a[3], b[1] + b[3]) - y
        return x, y, w, h

    @staticmethod
    def __rect_intersection(a: Tuple[int, int, int, int], b: Tuple[int, int, int, int]) -> Tuple:
        x = max(a[0], b[0])
        y = max(a[1], b[1])
        w = min(a[0] + a[2], b[0] + b[2]) - x
        h = min(a[1] + a[3], b[1] + b[3]) - y
        if w < 0 or h < 0:
            return ()  # or (0,0,0,0) ?
        return x, y, w, h

    @staticmethod
    def combine_rectangles(rects: list) -> np.ndarray:
        has_merged = False
        no_intersect_main = False
        # keep looping until we have completed a full pass over each rectangle
        # and checked it does not overlap with any other rectangle
        while no_intersect_main is False:
            no_intersect_main = True
            pos_index = 0
            # start with the first rectangle in the list, once the first
            # rectangle has been unioned with every other rectangle,
            # repeat for the second until done
            while pos_index < len(rects):
                no_intersect_loop = False
                while no_intersect_loop is False and len(rects) > 1:
                    a = rects[pos_index]
                    list_boxes = np.delete(rects, pos_index, 0)
                    index = 0
                    for b in list_boxes:
                        # if there is an intersection, the boxes overlap
                        if CombineRectangles.__rect_intersection(a, b):
                            has_merged = True
                            new_box = CombineRectangles.__rect_union(a, b)
                            list_boxes[index] = new_box
                            rects = list_boxes
                            no_intersect_loop = False
                            no_intersect_main = False
                            index = index + 1
                            break
                        no_intersect_loop = True
                        index = index + 1
                pos_index = pos_index + 1

        if has_merged is False:
            return np.array(rects)

        return rects.astype("int")
