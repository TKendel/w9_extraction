import pytesseract
import cv2
import os

from extraction.field_extraction import field_extraction

"""
Function for extracting all the needed data from the forms 
"""
for filename in os.listdir("/content/w9_extraction/data"):
    if filename.endswith(".jpg"):
        image = cv2.imread("/content/w9_extraction/data/{}".format(filename))
        image = cv2.resize(image, (4250, 5500), cv2.INTER_CUBIC)
        og_height, og_width = image.shape[:2]
        copy = image.copy()

        contours = field_extraction(image)

        found_fields = []
        for c in contours:
            rect = cv2.minAreaRect(c)
            box = cv2.boxPoints(rect)
            x, y, w, h = cv2.boundingRect(box)
            start_point = (x, y)
            end_point = (x + w, y + h)
            if og_width * 0.4 < w < og_width and og_height * 0.02 < h < og_height * 0.15:
                found_fields.append([x, y, w, h])

        cleaned_list = []
        for i in found_fields:
            if i not in cleaned_list:
                cleaned_list.append(i)

        position = 0
        switch = False
        for i, field in enumerate(reversed(cleaned_list)):
            x, y, w, h = field
            start_point = (x, y)
            end_point = (x + w, y + h)
            top_fields = []
            if 300 < h < 500 and y < og_height / 2:
                crop = image[y:y + h, x:x + w]
                image = cv2.rectangle(image, start_point, end_point, (0, 255, 0), 3)
            elif not h > 500 and y < og_height / 2:
                crop = image[y:y + h, x:x + w]
                text = pytesseract.image_to_string(crop, lang='eng', config='--psm 6')
                print(text)
                print(30 * "-")
                image = cv2.rectangle(image, start_point, end_point, (0, 0, 255), 3)

        cv2.imwrite('/content/images/fields.jpg', image)
