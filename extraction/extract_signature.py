import cv2
import os
from extraction.extract_bounding_boxes import ExtractBoundingBoxes

"""
Class for extracting signatures from forms by using the extracted bounding boxes and comparing them against a 
colour percentage
"""


class ExtractSignature():

    def extract_signature(self, image):
        bounding_boxes = ExtractBoundingBoxes()

        combined = bounding_boxes.extract_bounding_boxes([image])
        # load data from previous pipe
        for img, combined_rects, original in combined:

            signature_coordinates = []

            # rescale every bounding box in the manipulated image and draw them on the original image
            if combined_rects is not None:
                for (x, y, w, h) in combined_rects:

                    cropped = img[y:y + h, x:x + w]
                    percent = ExtractSignature.__confidence(cropped)

                    if percent > 60:
                        signature_coordinates.append([x, y, w, h])
                        cv2.rectangle(image, (x, y), (x + w, y + h), (0, 0, 255), thickness=4)

        cv2.imwrite('/content/images/signature.jpg', image)

    @staticmethod
    def __confidence(cropped):
        avg_white = 85.7378329549517

        ret, thresh = cv2.threshold(cropped, 127, 255, cv2.THRESH_BINARY)

        histogram = cv2.calcHist([thresh], [0], None, [256], [0, 256])

        white = histogram[255][0]
        black = histogram[0][0]

        total_sum = white + black

        perc_white = (white / total_sum) * 100

        confidence = (perc_white / avg_white) * 100

        return confidence


if __name__ == "__main__":
    for filename in os.listdir("/content/w9_extraction/data"):
        if filename.endswith(".jpg"):
            image = cv2.imread("/content/w9_extraction/data/{}".format(filename))
            image = cv2.resize(image, (4250, 5500), cv2.INTER_CUBIC)
            extract_numbers = ExtractSignature()
            extract_numbers.extract_signature(image)
