import cv2
import numpy as np
from helpers.boxification import boxification

"""
Function for extracting fields from forms
"""


def field_extraction(img):
    th1, img_bin = cv2.threshold(img, 150, 255, cv2.THRESH_BINARY)
    img_bin = ~img_bin

    # kerndels for horizontal and vertical lines
    line_width = 100
    kernel_h = np.ones((1, line_width), np.uint8)
    kernel_v = np.ones((line_width, 1), np.uint8)

    kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (3, 3))

    # finding the lines and combining them into one
    img_bin_h = cv2.morphologyEx(img_bin, cv2.MORPH_OPEN, kernel_h)
    img_bin_v = cv2.morphologyEx(img_bin, cv2.MORPH_OPEN, kernel_v)

    img_bin_final = img_bin_h | img_bin_v

    # making them more prominent
    dilate = cv2.dilate(img_bin_final, kernel, iterations=3)
    opening = cv2.morphologyEx(dilate, cv2.MORPH_CLOSE, (3, 3))
    closing = cv2.morphologyEx(opening, cv2.MORPH_OPEN, (3, 3))
    closing = ~closing

    # cv2.imwrite('/Users/tkendel/Documents/w9_extraction/line_combine.jpg', closing)

    # putting a big rectangle around the form
    boxification(closing)

    # cv2.imwrite('/Users/tkendel/Documents/w9_extraction/box.jpg', closing)

    # find fields
    canny = cv2.Canny(closing, 130, 255, 1)
    cnts, hierarchy = cv2.findContours(canny, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

    return cnts
