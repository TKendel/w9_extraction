import cv2
import imutils
import pytesseract
import re
import os

"""
Class for finding and extracting the data from serial number boxes
"""


class NumberExtraction():

    def number_extraction(self, image):
        copy = image.copy()
        gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        _, thresh = cv2.threshold(gray, 127, 255, cv2.THRESH_BINARY_INV)

        # find contours
        contours = cv2.findContours(thresh, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)
        contours = imutils.grab_contours(contours)

        for cnt in contours:
            rect = cv2.minAreaRect(cnt)
            box = cv2.boxPoints(rect)
            x, y, w, h = cv2.boundingRect(box)
            start_point = (x, y)
            end_point = (x + w, y + h)
            # if the contour is small enough and of rectangular shape, run ocr over it
            if 80 < w < 200 and 80 < h < 200:
                if NumberExtraction.__rectangle(cnt):
                    crop = image[y + 10:y + h - 10, x + 10:x + w - 10]
                    crop = cv2.cvtColor(crop, cv2.COLOR_BGR2GRAY)
                    threshold = cv2.threshold(crop, 128, 255, cv2.THRESH_BINARY)[1]
                    blur = cv2.blur(threshold, (3, 3))
                    erode = cv2.erode(blur, (3, 3), iterations=1)
                    closing = cv2.morphologyEx(erode, cv2.MORPH_CLOSE, (5, 5))
                    text = pytesseract.image_to_string(closing, lang='eng', config='--psm 6')
                    image = cv2.rectangle(copy, start_point, end_point, (0, 255, 0), 5)
                    cv2.putText(copy, re.sub("[^0-9]", "", text), (x + 5, y + 20), cv2.FONT_HERSHEY_SIMPLEX, 5,
                                (0, 0, 255), 4)

        cv2.imwrite('/content/w9_extraction/numbers.jpg', image)

    @staticmethod
    def __rectangle(c):
        peri = cv2.arcLength(c, True)
        approx = cv2.approxPolyDP(c, 0.02 * peri, True)

        return len(approx) == 4


if __name__ == "__main__":
    for filename in os.listdir("/content/w9_extraction/data"):
        if filename.endswith(".jpg"):
            image = cv2.imread("/content/w9_extraction/data/{}".format(filename))
            image = cv2.resize(image, (4250, 5500), cv2.INTER_CUBIC)
            extract_numbers = NumberExtraction()
            extract_numbers.number_extraction(image)
