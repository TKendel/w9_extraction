import cv2
import os
import numpy as np
import matplotlib.pyplot as plt
from skimage import measure, morphology
from skimage.measure import regionprops
from helpers.line_removal import line_removal
from helpers.combine_rectangles import CombineRectangles

"""
Class for extracting bounding boxes of possible signatures
"""


class ExtractBoundingBoxes():

    def extract_bounding_boxes(self, image):
        combine_rectangles = CombineRectangles()
        # this should save the image we change in the pipeline , if we change it
        original = image
        combined = []
        for og_image in original:
            img = cv2.cvtColor(og_image.copy(), cv2.COLOR_BGR2GRAY)
            img = line_removal(img)

            # ---- picture modification ----
            img = ExtractBoundingBoxes.__blurring(img)
            img = ExtractBoundingBoxes.__threshold_otsu(img)

            # connected component analysis by scikit-learn framework
            blobs = img > img.mean()
            blobs_labels = measure.label(blobs, background=1)

            the_biggest_component = 0
            total_area = 0
            counter = 0
            for region in regionprops(blobs_labels):
                if region.area > 10:
                    total_area = total_area + region.area
                    counter = counter + 1
                # print region.area # (for debugging)
                # take regions with large enough areas
                if region.area >= 250:
                    if region.area > the_biggest_component:
                        the_biggest_component = region.area

            average = (total_area / counter)
            print("the_biggest_component: " + str(the_biggest_component))
            print("average: " + str(average))

            # experimental-based ratio calculation, modify it for your cases
            # a4_constant is used as a threshold value to remove connected pixels
            # are smaller than a4_constant for A4 size scanned documents
            a4_constant = ((average / 84.0) * 250.0) + 100
            print("a4_constant: " + str(a4_constant))

            # remove the connected pixels are smaller than a4_constant
            b = morphology.remove_small_objects(blobs_labels, a4_constant)
            # save the the pre-version which is the image is labelled with colors
            # as considering connected components
            plt.imsave('pre_version.png', b)

            # read the pre-version
            img = cv2.imread('pre_version.png', 0)
            os.remove('pre_version.png')

            # ensure binary
            img = cv2.threshold(img, 0, 255, cv2.THRESH_BINARY_INV | cv2.THRESH_OTSU)[1]

            # ----- bounding box -----
            img = cv2.blur(img, ksize=(3, 3))
            kernel = np.ones((9, 9), np.uint8)
            img = cv2.erode(img, kernel=kernel, iterations=1)
            cnts, h = cv2.findContours(img, cv2.RETR_CCOMP, cv2.CHAIN_APPROX_SIMPLE)
            img = cv2.cvtColor(img, cv2.COLOR_GRAY2BGR)

            # Array of initial bounding rects
            rects = []

            # Just initialize bounding rects and set all bools to false
            for cnt in cnts:
                rect = cv2.boundingRect(cnt)
                # remove the possible contours of the paper form from the image
                # check found contours against the image width and height and discard any who are out of range
                if rect[0] > 0 and rect[1] > 0 and \
                        rect[2] <= (img.shape[1] / 2) and rect[3] <= (img.shape[0] / 3) and \
                        rect[2] > 30 and rect[3] > 15:
                    rects.append(rect)

            if len(rects) != 0:
                # merge all the overlapping bounding boxes
                combined_rects = combine_rectangles.combine_rectangles(rects)

                # package the coordinates with the image and the original image
                tagged_image = [img, combined_rects, og_image]
                combined.append(tagged_image)

            else:
                combined_rects = None
                tagged_image = [img, combined_rects, og_image]
                combined.append(tagged_image)

        return combined

    @staticmethod
    def __resize_width(standard_width, img):
        r = standard_width / img.shape[1]
        dim = (standard_width, int(img.shape[0] * r))
        return cv2.resize(img, dim, interpolation=cv2.INTER_CUBIC)

    @staticmethod
    def __resize_height(standard_height, img):
        dim = (img.shape[1], standard_height)
        return cv2.resize(img, dim, interpolation=cv2.INTER_CUBIC)

    @staticmethod
    def __erosion_after_dialation(img):
        kernel = np.ones((17, 17), np.uint8)
        return cv2.morphologyEx(img, cv2.MORPH_OPEN, kernel)

    @staticmethod
    def __erosion(img):
        kernel = np.ones((3, 3), np.uint8)
        return cv2.erode(img, kernel, iterations=2)

    @staticmethod
    def __blurring(img):
        return cv2.blur(img, ksize=(5, 5))

    @staticmethod
    def __threshold_otsu(img):
        blur = cv2.GaussianBlur(img, (5, 5), 0)
        return cv2.threshold(blur, 127, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)[1]

    @staticmethod
    def __threshold_adaptive(img):
        return cv2.threshold(img, 127, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, 11, 2)
